﻿/// Das ist, wie die anderen beiden Files auch, eine Abwandlung der 
/// Game-of-Life-Implementierung von L. Brüggemann.
/// 
/// (c) Lennart Brüggemann, https://gist.github.com/lennartb-/7482783
/// Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)

namespace WhatDoesThisDo

{
    public class GameOfLiveSimulation
    {
        private int Heigth;
        private int Width;
        private bool[,] cells;

        public GameOfLiveSimulation(int Heigth, int Width)
        {
            this.Heigth = Heigth;
            this.Width = Width;
            cells = new bool[Heigth, Width];
            GenerateInitialCellDistribution();
        }

        public void NextStepInSimulation()
        {
            Draw();
            GrowNextGeneration();
        }

        private void GrowNextGeneration()
        {
            for (int i = 0; i < Heigth; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    int numOfAliveNeighbors = NumberOfAliveNeighbours(i, j);

                    if (cells[i, j])
                    {
                        if (numOfAliveNeighbors < 2)
                        {
                            cells[i, j] = false;
                        }

                        if (numOfAliveNeighbors > 3)
                        {
                            cells[i, j] = false;
                        }
                    }
                    else
                    {
                        if (numOfAliveNeighbors == 3)
                        {
                            cells[i, j] = true;
                        }
                    }
                }
            }
        }

        private int NumberOfAliveNeighbours(int x, int y)
        {
            int NumOfAliveNeighbors = 0;

            for (int i = x - 1; i < x + 2; i++)
            {
                for (int j = y - 1; j < y + 2; j++)
                {
                    if (!((i < 0 || j < 0) || (i >= Heigth || j >= Width)))
                    {
                        if (cells[i, j] == true) NumOfAliveNeighbors++;
                    }
                }
            }
            return NumOfAliveNeighbors;
        }

        private void Draw()
        {
            for (int i = 0; i < Heigth; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    Console.Write(cells[i, j] ? "x" : " ");
                    if (j == Width - 1) Console.WriteLine("\r");
                }
            }
            Console.SetCursorPosition(0, Console.WindowTop);
        }

        private void GenerateInitialCellDistribution()
        {
            Random generator = new Random();
            int number;
            for (int i = 0; i < Heigth; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    number = generator.Next(10);
                    cells[i, j] = ((number == 0) ? false : true);
                }
            }
        }
    }
}