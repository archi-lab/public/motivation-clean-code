﻿namespace WhatDoesThisDo

{
    public class Simulation
    {
        private int Heigth;
        private int Width;
        private bool[,] cells;

        public Simulation(int Heigth, int Width)
        {
            this.Heigth = Heigth;
            this.Width = Width;
            cells = new bool[Heigth, Width];
            Init();
        }

        public void DrawAndGrow()
        {
            Draw();
            Grow();
        }

        private void Grow()
        {
            for (int i = 0; i < Heigth; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    int numAlive = GetNeighbors(i, j);

                    if (cells[i, j])
                    {
                        if (numAlive < 2)
                        {
                            cells[i, j] = false;
                        }

                        if (numAlive > 3)
                        {
                            cells[i, j] = false;
                        }
                    }
                    else
                    {
                        if (numAlive == 3)
                        {
                            cells[i, j] = true;
                        }
                    }
                }
            }
        }

        private int GetNeighbors(int x, int y)
        {
            int numAlive = 0;

            for (int i = x - 1; i < x + 2; i++)
            {
                for (int j = y - 1; j < y + 2; j++)
                {
                    if (!((i < 0 || j < 0) || (i >= Heigth || j >= Width)))
                    {
                        if (cells[i, j] == true) numAlive++;
                    }
                }
            }
            return numAlive;
        }

        private void Draw()
        {
            for (int i = 0; i < Heigth; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    Console.Write(cells[i, j] ? "x" : " ");
                    if (j == Width - 1) Console.WriteLine("\r");
                }
            }
            Console.SetCursorPosition(0, Console.WindowTop);
        }

        private void Init()
        {
            Random rd = new Random();
            int number;
            for (int i = 0; i < Heigth; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    number = rd.Next(10);
                    cells[i, j] = ((number == 0) ? false : true);
                }
            }
        }
    }
}