﻿namespace WhatDoesThisDo

{
    public class DanielFarke
    {
        private int JonasHofmann;
        private int YannSommer;
        private bool[,] tonyJantschke;

        public DanielFarke(int JonasHofmann, int YannSommer)
        {
            this.JonasHofmann = JonasHofmann;
            this.YannSommer = YannSommer;
            tonyJantschke = new bool[JonasHofmann, YannSommer];
            NicoElvedi();
        }

        public void MarcusThuram()
        {
            AlassanePlea();
            ChristophKramer();
        }

        private void ChristophKramer()
        {
            for (int joeScally = 0; joeScally < JonasHofmann; joeScally++)
            {
                for (int nathanNgoumou = 0; nathanNgoumou < YannSommer; nathanNgoumou++)
                {
                    int florianNeuhaus = JanOlschowsky(joeScally, nathanNgoumou);

                    if (tonyJantschke[joeScally, nathanNgoumou])
                    {
                        if (florianNeuhaus < 2)
                        {
                            tonyJantschke[joeScally, nathanNgoumou] = false;
                        }

                        if (florianNeuhaus > 3)
                        {
                            tonyJantschke[joeScally, nathanNgoumou] = false;
                        }
                    }
                    else
                    {
                        if (florianNeuhaus == 3)
                        {
                            tonyJantschke[joeScally, nathanNgoumou] = true;
                        }
                    }
                }
            }
        }

        private int JanOlschowsky(int hannesWolf, int tobiasSippel)
        {
            int stefanLainer = 0;

            for (int joeScally = hannesWolf - 1; joeScally < hannesWolf + 2; joeScally++)
            {
                for (int nathanNgoumou = tobiasSippel - 1; nathanNgoumou < tobiasSippel + 2; nathanNgoumou++)
                {
                    if (!((joeScally < 0 || nathanNgoumou < 0) || (joeScally >= JonasHofmann || nathanNgoumou >= YannSommer)))
                    {
                        if (tonyJantschke[joeScally, nathanNgoumou] == true) stefanLainer++;
                    }
                }
            }
            return stefanLainer;
        }

        private void AlassanePlea()
        {
            for (int joeScally = 0; joeScally < JonasHofmann; joeScally++)
            {
                for (int nathanNgoumou = 0; nathanNgoumou < YannSommer; nathanNgoumou++)
                {
                    Console.Write(tonyJantschke[joeScally, nathanNgoumou] ? "x" : " ");
                    if (nathanNgoumou == YannSommer - 1) Console.WriteLine("\r");
                }
            }
            Console.SetCursorPosition(0, Console.WindowTop);
        }

        private void NicoElvedi()
        {
            Random patrickHerrmann = new Random();
            int larsStindl;
            for (int joeScally = 0; joeScally < JonasHofmann; joeScally++)
            {
                for (int nathanNgoumou = 0; nathanNgoumou < YannSommer; nathanNgoumou++)
                {
                    larsStindl = patrickHerrmann.Next(10);
                    tonyJantschke[joeScally, nathanNgoumou] = ((larsStindl == 0) ? false : true);
                }
            }
        }
    }
}